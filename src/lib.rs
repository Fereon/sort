//! # Sort
//!
//! `sort` is a collection of implementations of several sorting algorithms.
//! This is just a little test project for practice purposes.

mod bubble;
mod heap;
mod insertion;
mod merge;
mod quick;

pub use bubble::sort as bubble_sort;
pub use heap::sort as heap_sort;
pub use insertion::sort as insertion_sort;
pub use merge::sort as merge_sort;
pub use quick::sort as quick_sort;

#[cfg(test)]
mod test_collection {
    use rand::distributions::Standard;
    use rand::prelude::*;

    pub fn empty_is_sorted<F>(f: F)
    where
        F: Fn(&mut [char]),
    {
        let mut arr: [char; 0] = [];
        let test_arr = arr.clone();

        f(&mut arr);

        assert_eq!(test_arr, arr);
    }

    pub fn one_element_is_sorted<F>(f: F)
    where
        F: Fn(&mut [f64]),
    {
        let mut arr = [1.5];
        let test_arr = arr.clone();

        f(&mut arr);

        assert_eq!(test_arr, arr);
    }

    pub fn already_ordered_stays_ordered<F>(f: F)
    where
        F: Fn(&mut [i32]),
    {
        let mut arr = [1, 1, 2, 3, 5, 8, 13, 15];
        let test_arr = arr.clone();

        f(&mut arr);

        assert_eq!(test_arr, arr);
    }

    pub fn does_sort<F>(f: F)
    where
        F: Fn(&mut [i32]),
    {
        let mut arr = [13, 1, 2, 1, 5, 8, 3];
        let test_arr = [1, 1, 2, 3, 5, 8, 13];

        f(&mut arr);

        assert_eq!(test_arr, arr);
    }

    pub fn does_sort_random_samples<F, T>(f: F)
    where
        F: Fn(&mut [T]),
        T: PartialOrd,
        Standard: rand::distributions::Distribution<T>,
    {
        let mut rng = rand::thread_rng();

        for i in 0..500 {
            let mut items: Vec<T> = (&mut rng).sample_iter::<T, _>(Standard).take(i).collect();

            f(&mut items);

            assert_eq!(items.len(), i);
            assert!(is_sorted::<T>(&items));
        }
    }

    pub fn is_sorted<T: PartialOrd>(arr: &[T]) -> bool {
        arr.iter()
            .fold((None, true), |(prev, result), cur| {
                match ((prev, result), cur) {
                    ((None, _), cur) => (Some(cur), true),
                    ((prev, false), _) => (prev, false),
                    ((Some(prev), true), cur) => (Some(cur), prev <= cur),
                }
            })
            .1
    }
}

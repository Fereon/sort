/// Sorts an array or slice using merge sort.
/// This algorithm is stable and has a complexity of O(n log(n)).
///
/// # Example
///
/// ```
/// use sort::merge_sort;
///
/// let mut arr = [1.5, 8.9, 5.4, 10.33, 7.3, 5.2];
/// merge_sort(&mut arr);
///
/// assert_eq!([1.5, 5.2, 5.4, 7.3, 8.9, 10.33], arr);
/// ```
pub fn sort<T: PartialOrd + Copy>(arr: &mut [T]) {
    if arr.len() > 1 {
        let m = arr.len() / 2;

        let arr_left = &mut arr[..m];
        sort(arr_left);

        let arr_right = &mut arr[m..];
        sort(arr_right);

        merge(arr, m);
    }
}

fn merge<T: PartialOrd + Copy>(arr: &mut [T], m: usize) {
    // Making a copy of the slice
    let len = arr.len();
    let mut arr_copy: Vec<T> = Vec::with_capacity(len);
    arr_copy.extend_from_slice(arr);

    // Shuffling
    let mut left: usize = 0;
    let mut right: usize = m;

    for elem in arr {
        if left < m && right < len {
            if arr_copy[left] <= arr_copy[right] {
                *elem = arr_copy[left];
                left += 1;
            } else {
                *elem = arr_copy[right];
                right += 1;
            }
        } else if right < len {
            *elem = arr_copy[right];
            right += 1;
        } else {
            *elem = arr_copy[left];
            left += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn merges_correctly_even() {
        let mut arr1: [i32; 4] = [2, 4, 3, 5];
        merge(&mut arr1, 2);
        assert_eq!([2, 3, 4, 5], arr1);
    }

    #[test]
    fn merges_correctly_odd() {
        let mut arr2: [i32; 3] = [1, 2, 3];
        merge(&mut arr2, 1);
        assert_eq!([1, 2, 3], arr2);
    }

    #[test]
    fn merges_correctly_left() {
        let mut arr3: [i32; 4] = [1, 2, 3, 4];
        merge(&mut arr3, 2);
        assert_eq!([1, 2, 3, 4], arr3);
    }

    #[test]
    fn merges_correctly_right() {
        let mut arr4: [i32; 5] = [3, 4, 3, 3, 5];
        merge(&mut arr4, 2);
        assert_eq!([3, 3, 3, 4, 5], arr4);
    }

    #[test]
    fn empty_is_sorted() {
        crate::test_collection::empty_is_sorted(sort);
    }

    #[test]
    fn one_element_is_sorted() {
        crate::test_collection::one_element_is_sorted(sort);
    }

    #[test]
    fn already_ordered_stays_ordered() {
        crate::test_collection::already_ordered_stays_ordered(sort);
    }

    #[test]
    fn does_sort() {
        crate::test_collection::does_sort(sort);
    }

    #[test]
    fn does_sort_random_samples() {
        crate::test_collection::does_sort_random_samples::<_, i32>(sort);
        crate::test_collection::does_sort_random_samples::<_, char>(sort);
        crate::test_collection::does_sort_random_samples::<_, f64>(sort);
    }
}

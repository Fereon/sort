/// Sorts an array or slice using heap sort.
/// This algorithm is not stable and has a complexity of O(n log(n)).
///
/// # Example
///
/// ```
/// use sort::heap_sort;
///
/// let mut arr = [1.6, 8.9, 5.8, 10.33, 7.3, 5.2];
/// heap_sort(&mut arr);
///
/// assert_eq!([1.6, 5.2, 5.8, 7.3, 8.9, 10.33], arr);
/// ```
pub fn sort<T: PartialOrd>(arr: &mut [T]) {
    let mut heap = MaxHeap::new(arr);

    // unravels heap again
    for last in (0..heap.arr.len()).rev() {
        heap.arr.swap(0, last);
        heap.heapify(last, 0);
    }
}

struct MaxHeap<'a, T: PartialOrd> {
    arr: &'a mut [T],
}

impl<'a, T: PartialOrd> MaxHeap<'a, T> {
    fn new(arr: &'a mut [T]) -> MaxHeap<'a, T> {
        let n = arr.len();
        let mut new_heap = MaxHeap { arr };

        // makes sure the heap is a heap
        for i in (0..n / 2).rev() {
            new_heap.heapify(n, i)
        }

        new_heap
    }

    fn heapify(&mut self, k: usize, i: usize) {
        let has_left = Self::has_left_child(k, i);
        let has_right = Self::has_right_child(k, i);

        let child = match (has_left, has_right) {
            (false, false) => return,
            (true, false) => Self::left(i),
            (false, true) => Self::right(i),
            (true, true) => {
                if self.arr[Self::left(i)] > self.arr[Self::right(i)] {
                    Self::left(i)
                } else {
                    Self::right(i)
                }
            }
        };

        if self.arr[child] > self.arr[i] {
            self.arr.swap(child, i);
            self.heapify(k, child);
        }
    }

    fn left(i: usize) -> usize {
        2 * i + 1
    }

    fn right(i: usize) -> usize {
        2 * i + 2
    }

    fn has_left_child(k: usize, i: usize) -> bool {
        k > 1 && i <= k / 2 - 1
    }

    fn has_right_child(k: usize, i: usize) -> bool {
        k > 2 && i <= (k + 1) / 2 - 2
    }
}

#[cfg(test)]
mod tests {
    use super::{sort, MaxHeap};

    #[test]
    fn heapifies() {
        let mut arr = [27, 65, 42, 56, 22, 17, 13, 37];
        let test_arr = [65, 56, 42, 37, 22, 17, 13, 27];

        let MaxHeap { arr } = MaxHeap::new(&mut arr);

        assert_eq!(arr, test_arr);
    }

    #[test]
    fn empty_is_sorted() {
        crate::test_collection::empty_is_sorted(sort);
    }

    #[test]
    fn one_element_is_sorted() {
        crate::test_collection::one_element_is_sorted(sort);
    }

    #[test]
    fn already_ordered_stays_ordered() {
        crate::test_collection::already_ordered_stays_ordered(sort);
    }

    #[test]
    fn does_sort() {
        crate::test_collection::does_sort(sort);
    }

    #[test]
    fn does_sort_random_samples() {
        crate::test_collection::does_sort_random_samples::<_, i32>(sort);
        crate::test_collection::does_sort_random_samples::<_, char>(sort);
        crate::test_collection::does_sort_random_samples::<_, f64>(sort);
    }
}

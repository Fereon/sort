/// Sorts an array or slice using quicksort.
/// This algorithm has an average complexity of O(n log (n)) and a
/// worst case complexity of (n²). This sorting algorithm
/// is not stable.
///
/// # Example
/// ```
/// use sort::quick_sort;
///
/// let mut arr = [5, 8, 17, 1, 12, 2, 2, 21];
/// quick_sort(&mut arr);
///
/// assert_eq!([1, 2, 2, 5, 8, 12, 17, 21], arr);
/// ```
pub fn sort<T: PartialOrd>(arr: &mut [T]) {
    if arr.len() > 1 {
        let p = partition(arr);
        sort(&mut arr[..p]);
        sort(&mut arr[p + 1..]);
    }
}

fn partition<T: PartialOrd>(arr: &mut [T]) -> usize {
    let len = arr.len();
    let last = len - 1;

    // choose pivot and swap to end
    let p = heuristic_median(arr);
    arr.swap(p, last);

    // start partitioning
    let mut i: usize = 0;
    for j in 0..len - 1 {
        if arr[j] <= arr[last] {
            arr.swap(i, j);
            i += 1;
        }
    }

    // swap pivot back into position
    arr.swap(i, last);
    i
}

// simple best of three implementation in O(1)
fn heuristic_median<T: PartialOrd>(arr: &[T]) -> usize {
    let middle = arr.len() / 2;
    let last = arr.len() - 1;

    let a = &arr[0];
    let b = &arr[middle];
    let c = &arr[last];

    if (a > b) ^ (a > c) {
        0
    } else if (b < a) ^ (b < c) {
        middle
    } else {
        last
    }
}

#[cfg(test)]
mod tests {
    use super::{heuristic_median, partition, sort};

    #[test]
    fn best_median_first() {
        let arr = [2, 1, 3];
        let m = heuristic_median(&arr);
        assert_eq!(2, arr[m]);
    }

    #[test]
    fn best_median_middle() {
        let arr = [1, 2, 3];
        let m = heuristic_median(&arr);
        assert_eq!(2, arr[m]);
    }

    #[test]
    fn best_median_last() {
        let arr = [2, 1, 3];
        let m = heuristic_median(&arr);
        assert_eq!(2, arr[m]);
    }

    #[test]
    fn empty_is_sorted() {
        crate::test_collection::empty_is_sorted(sort);
    }

    #[test]
    fn does_partition() {
        let mut arr = [22, 24, 3, 29, 12, 11, 18];

        let p = partition(&mut arr);

        assert!(
            arr[..p].iter().fold(true, |b, x| b && x <= &arr[p]),
            "Found elements greater than the pivot element before its position"
        );
        assert!(
            arr[p + 1..].iter().fold(true, |b, x| b && x >= &arr[p]),
            "Found elements smaller than the pivot element after its position"
        );
    }

    #[test]
    fn one_element_is_sorted() {
        crate::test_collection::one_element_is_sorted(sort);
    }

    #[test]
    fn already_ordered_stays_ordered() {
        crate::test_collection::already_ordered_stays_ordered(sort);
    }

    #[test]
    fn does_sort() {
        crate::test_collection::does_sort(sort);
    }

    #[test]
    fn does_sort_random_samples() {
        crate::test_collection::does_sort_random_samples::<_, i32>(sort);
        crate::test_collection::does_sort_random_samples::<_, char>(sort);
        crate::test_collection::does_sort_random_samples::<_, f64>(sort);
    }
}

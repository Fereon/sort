/// Sorts an array or slice using bubble sort.
/// Although stable, this algorithm is generally a quite slow and in O(n²).
///
/// # Examples
/// ```
/// use sort::bubble_sort;
///
/// let mut arr = [18, 16, 26, 2, 19, 0, 28];
/// bubble_sort(&mut arr);
///
/// assert_eq!([0, 2, 16, 18, 19, 26, 28], arr);
/// ```
pub fn sort<T: PartialOrd>(arr: &mut [T]) {
    for n in (1..arr.len()).rev() {
        for i in 0..n {
            if arr[i] > arr[i + 1] {
                arr.swap(i, i + 1)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::sort;

    #[test]
    fn empty_is_sorted() {
        crate::test_collection::empty_is_sorted(sort);
    }

    #[test]
    fn one_element_is_sorted() {
        crate::test_collection::one_element_is_sorted(sort);
    }

    #[test]
    fn already_ordered_stays_ordered() {
        crate::test_collection::already_ordered_stays_ordered(sort);
    }

    #[test]
    fn does_sort() {
        crate::test_collection::does_sort(sort);
    }

    #[test]
    fn does_sort_random_samples() {
        crate::test_collection::does_sort_random_samples::<_, i32>(sort);
        crate::test_collection::does_sort_random_samples::<_, char>(sort);
        crate::test_collection::does_sort_random_samples::<_, f64>(sort);
    }
}

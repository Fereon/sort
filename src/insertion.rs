/// Sorts an array or slice using insertion sort.
/// This algorithm is stable and quite efficient for smaller sets,
/// but has an overall complexity of O(n²).
///
/// # Example
/// ```
/// use sort::insertion_sort;
///
/// let mut arr = ['T', 'L', 'u', 'A', 'J', 'q', 'M', 'V', 'q'];
/// insertion_sort(&mut arr);
///
/// assert_eq!(['A', 'J', 'L', 'M', 'T', 'V', 'q', 'q', 'u'], arr);
/// ```
pub fn sort<T: PartialOrd + Copy>(arr: &mut [T]) {
    for i in 0..arr.len() {
        let val = arr[i];
        let mut j = i;
        while j > 0 && arr[j - 1] > val {
            arr[j] = arr[j - 1];
            j -= 1;
        }
        arr[j] = val;
    }
}

#[cfg(test)]
mod tests {
    use super::sort;

    #[test]
    fn empty_is_sorted() {
        crate::test_collection::empty_is_sorted(sort);
    }

    #[test]
    fn one_element_is_sorted() {
        crate::test_collection::one_element_is_sorted(sort);
    }

    #[test]
    fn already_ordered_stays_ordered() {
        crate::test_collection::already_ordered_stays_ordered(sort);
    }

    #[test]
    fn does_sort() {
        crate::test_collection::does_sort(sort);
    }

    #[test]
    fn does_sort_random_samples() {
        crate::test_collection::does_sort_random_samples::<_, i32>(sort);
        crate::test_collection::does_sort_random_samples::<_, char>(sort);
        crate::test_collection::does_sort_random_samples::<_, f64>(sort);
    }
}
